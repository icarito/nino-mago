#!/bin/env python3

from bibcast import Bibcast
from subprocess import call

call (['yarn', 'run', 'build'])
with open("index.html", "w") as index:
    index.write(Bibcast().render())

